
# Kubernetes/Kubespray

## Prepare machines

### Kubernetes machines (4)

* CentOS
* 8GB RAM/ 4 Cores / 100GB HD
* make sure the machines are SSH-able with a same key without passphrase under a user with root access (can be root)
* make sure the machines have internet access

### Admin machine (1)

* machine only needs to be live when installing the Kubernetes cluster
* can be virtual or physical
* can be reused for any environment
* CentOS
* 8GB RAM/ 4 Cores / 100GB HD
* install the SSH key used above in the Kubernetes machines and make sure all Kubernetes machines are SSH-able without password and without certificate warning (just SSH once and accept)

```bash
# make sure CentOS EPEL repos is installed
sudo yum install epel-release

# install Ansible (needed for Kubespray)
sudo yum install ansible

# install Git (needed for getting Ansible templates)
sudo yum install git

# install python pip (needed for Kubespray)
sudo yum install python-pip

# install Kubespray
sudo pip2 install kubespray

# update Jinja (or the cert_sync from the Ansible playbook will fail)
pip install --upgrade Jinja2

# install kubectl
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubectl

# get Istio binaries
curl -L https://git.io/getLatestIstio | sh -
cd istio-1.0.5
export PATH=$PWD/bin:$PATH
cd ..

# install Helm
curl -sL https://storage.googleapis.com/kubernetes-helm/helm-v2.12.1-linux-amd64.tar.gz | tar xz
mv linux-amd64/helm /usr/local/bin/helm
rm -rf linux-amd64
```

## Check Admin machine (142.93.142.69, GOCA 192.168.200.175 )

```bash
# check ssh access to nodes
# copy openssh exported PuttyGen to ~/.ssh/id_rsa)
chmod 400 ~/.ssh/id_rsa

# node1
ssh root@142.93.135.125
# node2
ssh root@142.93.228.103
# node3
ssh root@178.128.252.33
# node4
ssh root@142.93.228.152

# GOCA

# node1
ssh root@192.168.200.166
# node2
ssh root@192.168.200.167
# node3
ssh root@192.168.200.168
# node4
ssh root@192.168.200.169

# configure 'raw' Ansible
cat /etc/ansible/hosts
cat <<EOF > /etc/ansible/hosts
[nodes]
node1 ansible_ssh_host=142.93.135.125
node2 ansible_ssh_host=142.93.228.103
node3 ansible_ssh_host=178.128.252.33
node4 ansible_ssh_host=142.93.228.152
EOF

# configure 'raw' Ansible GOCA
cat /etc/ansible/hosts
cat <<EOF > /etc/ansible/hosts
[nodes]
node1 ansible_ssh_host=192.168.200.166
node2 ansible_ssh_host=192.168.200.167
node3 ansible_ssh_host=192.168.200.168
node4 ansible_ssh_host=192.168.200.169
EOF

# check if Ansible can reach all hosts
ansible -m ping all
# or
ansible -m ping nodes

# send a few commands to a node
ansible -m shell -a 'free -m' node1
ansible -m shell -a 'ls' node1

# check pip
pip --version

# check Kubespray
kubespray -v

# check Git
git --version
```

## Prepare  Kubespray

```bash
kubespray prepare --assumeyes --path ~/.kubespray  --nodes node1[ansible_ssh_host=142.93.135.125] node2[ansible_ssh_host=142.93.228.103] node3[ansible_ssh_host=178.128.252.33] node4[ansible_ssh_host=142.93.228.152] --masters node1 node2 --etcds node1 node2 node3

# GOCA
# proxy? proxy=w.x.y.z
kubespray prepare --assumeyes --path ~/.kubespray  --nodes node1[ansible_ssh_host=192.168.200.166] node2[ansible_ssh_host=192.168.200.167] node3[ansible_ssh_host=192.168.200.168] node4[ansible_ssh_host=192.168.200.169] --masters node1 node2 --etcds node1 node2 node3
```

### Optional: change vars

* in ``/root/.kubespray/inventory/sample/group_vars/k8s-cluster/k8s-cluster.yaml``
* adjust vars if needed (see <https://github.com/kubernetes-sigs/kubespray/blob/master/docs/vsphere.md>)

## Deploy Kubernetes

```bash
kubespray deploy --assumeyes
```

### In case of errors

```bash
ansible-playbook -i inventory/inventory.cfg cluster.yml -u root --limit @/root/.kubespray/cluster.retry
```

## Post deploy

```bash
# copy config from master to ~/.kube folder on Admin machine
mkdir -p ~/.kube
scp root@142.93.135.125:~/.kube/config ~/.kube

# check nodes
kubectl get nodes

# check pods
kubectl get pods --all-namespaces

# get k8s-ks
git clone https://gitlab.com/devgem/k8s-ks.git
# add admin user
kubectl apply -f k8s-ks/admin-user.yaml
# get admin token
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')

# token: eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLWM1Y3J2Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJlMDcwYzRiYi0xOGY2LTExZTktOTNlMC02YTQ1ZWNhZTUyNWYiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.WifpTnJv8nx5VLAElmGFgAgOWk0UpSVlL-RCvQ0QF2hcxR_msFaZKcVNGBHFocaK_vFtbL3iqumokoyHyc19CE6PWUipnJgrO0otHpWX3e0E92F1Un9yAXpiypGthbnaWOjcCKuFKhMLrPbq9_zuaXJtrwJz29rhGEZ7m_D6nitokgYhg3L9_S-_5xLM5SZ_ei4MfY5pj4qdM-PdOfycl_Nzo7l2fFSls_dKuTR4WVqHfXmszzqOxm-O7N8PZTrh472_Ec1hwp6TOjciqb4kH-C2yT6qYtxbBaTY5cNPiDfx9GyndsWHBeVxYzLRguiNBMBQOQllkrJOrv5J4vVh_g

# start proxy on machine with browser and config
kubectl --kubeconfig=config proxy
```

check out dashboard

<http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview>

or

<http://127.0.0.1:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview>

# Istio

```bash

# Istio install with helm fails a lot due to timing issues => use kubectl (can be retried)
kubectl apply -f install/kubernetes/istio-demo-auth.yaml

# add tiller user from Istio
cd istio-1.0.5
kubectl apply -f install/kubernetes/helm/helm-service-account.yaml

# install Helm with tiller user
helm init --service-account tiller

# install Istio
helm install install/kubernetes/helm/istio --name istio --namespace istio-system

# if it fails
kubectl delete crd gateways.networking.istio.io
kubectl delete crd adapters.config.istio.io
kubectl delete crd apikeys.config.istio.io
kubectl delete crd attributemanifests.config.istio.io
kubectl delete crd authorizations.config.istio.io
kubectl delete crd bypasses.config.istio.io
kubectl delete crd checknothings.config.istio.io
kubectl delete crd circonuses.config.istio.io
kubectl delete crd deniers.config.istio.io
kubectl delete crd destinationrules.networking.istio.io
kubectl delete crd edges.config.istio.io
kubectl delete crd envoyfilters.networking.istio.io
kubectl delete crd fluentds.config.istio.io
kubectl delete crd handlers.config.istio.io
kubectl delete crd httpapispecbindings.config.istio.io
kubectl delete crd httpapispecs.config.istio.io
kubectl delete crd instances.config.istio.io
kubectl delete crd kubernetesenvs.config.istio.io
kubectl delete crd kuberneteses.config.istio.io
kubectl delete crd listcheckers.config.istio.io
kubectl delete crd listentries.config.istio.io
kubectl delete crd logentries.config.istio.io
kubectl delete crd memquotas.config.istio.io
kubectl delete crd metrics.config.istio.io
kubectl delete crd noops.config.istio.io
kubectl delete crd opas.config.istio.io
kubectl delete crd prometheuses.config.istio.io
kubectl delete crd quotas.config.istio.io
kubectl delete crd quotaspecbindings.config.istio.io
kubectl delete crd quotaspecs.config.istio.io
kubectl delete crd rbacconfigs.rbac.istio.io
kubectl delete crd rbacs.config.istio.io
kubectl delete crd redisquotas.config.istio.io
kubectl delete crd reportnothings.config.istio.io
kubectl delete crd rules.config.istio.io
kubectl delete crd servicecontrolreports.config.istio.io
kubectl delete crd servicecontrols.config.istio.io
kubectl delete crd serviceentries.networking.istio.io
kubectl delete crd servicerolebindings.rbac.istio.io
kubectl delete crd serviceroles.rbac.istio.io
kubectl delete crd signalfxs.config.istio.io
kubectl delete crd solarwindses.config.istio.io
kubectl delete crd stackdrivers.config.istio.io
kubectl delete crd statsds.config.istio.io
kubectl delete crd stdios.config.istio.io
kubectl delete crd templates.config.istio.io
kubectl delete crd tracespans.config.istio.io
kubectl delete crd virtualservices.networking.istio.io
# and retry

# check ingress gateway
kubectl get svc istio-ingressgateway -n istio-system
# if keeps being pending then use NodePort of Service
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o 'jsonpath={.items[0].status.hostIP}')
export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT
echo $INGRESS_PORT
echo $SECURE_INGRESS_PORT
echo $INGRESS_HOST
echo $GATEWAY_URL

# 142.93.228.152:31380
```

```bash
# signal automatic sidecar injection
kubectl label namespace default istio-injection=enabled

cd istio-1.0.5

# deploy bookinfo
kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml

# add gateway
kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
```

access product page

<http://142.93.228.152:31380/productpage>

```bash
# check meshpolicy
kubectl get meshpolicy default -o yaml
# destination rules (no Mutual TLS)
kubectl apply -f samples/bookinfo/networking/destination-rule-all.yaml
# destination rules (Mutual TLS)
kubectl apply -f samples/bookinfo/networking/destination-rule-all-mtls.yaml
# check rules
kubectl get destinationrules -o yaml

# route all to v1
kubectl apply -f samples/bookinfo/networking/virtual-service-all-v1.yaml
# check virtual service
kubectl get virtualservices -o yaml
```

## HTTPS

https://istio.io/docs/tasks/traffic-management/secure-ingress/

# Volumes

<https://vmware.github.io/vsphere-storage-for-kubernetes/documentation/storageclass.html>

<https://github.com/kubernetes-sigs/kubespray/blob/master/docs/vsphere.md>